# Regex Test

This is a demo project that demonstrates a bug: If you have a library in your project and have a component that has a static regular expression field, you will get compiler errors while using regex literals. If you use the `RegExp` constructor instead to create your regex, it compiles.

## Steps to reproduce:

1. Open `./projects/my-lib/src/lib/my-lib.component.ts`
2. Un-comment the line with the regex literal (`static regex = /asdf/gi;`)
3. Try to `ng build my-lib` => You will get an error: "Metadata collected contains an error that will be reported at runtime: Expression form not supported."
4. Comment the line you un-commented above.
5. Un-comment the line with the regex constructor (`static regex2 = new RegExp("asdf", "gi");`)
6. try to `ng build my-lib` again. You will see that this time you can build successfully.
