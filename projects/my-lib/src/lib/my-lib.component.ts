import { Component, OnInit } from '@angular/core';

@Component({
  selector: "lib-my-lib",
  template: `
    <p>
      my-lib works!
    </p>
  `,
  styles: []
})
export class MyLibComponent implements OnInit {
  // this does not work, try `ng build my-lib`
  // static regex = /asdf/gi;

  // this instead works:
  // static regex2 = new RegExp("asdf", "gi");

  constructor() {}

  ngOnInit() {}
}
